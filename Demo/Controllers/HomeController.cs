﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Demo.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SLAPage()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public object GetData(int type)
        {
            try
            {
                if (type == 1)
                {
                    var data = new List<DataEntity>
                {
                    new DataEntity { x = "1PM", y = "85.4", z="60.92", title="SLA - Buzz" },
                    new DataEntity { x = "2PM", y = "92.7", z="60.92", title="SLA - Buzz" },
                    new DataEntity { x = "3PM", y = "64.9", z="60.92", title="SLA - Buzz" },
                    new DataEntity { x = "4PM", y = "58.0", z="60.92", title="SLA - Buzz" },
                    new DataEntity { x = "5PM", y = "45.0", z="78.92", title="SLA - Buzz" },
                    new DataEntity { x = "6PM", y = "23.0", z="78.92", title="SLA - Buzz" },
                    new DataEntity { x = "7PM", y = "23.0", z="78.92", title="SLA - Buzz" },
                    new DataEntity { x = "8PM", y = "78.0", z="78.92", title="SLA - Buzz" },
                    new DataEntity { x = "9PM", y = "46.0", z="78.92", title="SLA - Buzz" },

                    new DataEntity { x = "1PM", y = "32.3", z="60.92", title="SLA Calls" },
                    new DataEntity { x = "2PM", y = "33.9", z="60.92", title="SLA Calls" },
                    new DataEntity { x = "3PM", y = "34.3", z="60.92", title="SLA Calls" },
                    new DataEntity { x = "4PM", y = "29.6", z="60.92", title="SLA Calls" },
                    new DataEntity { x = "5PM", y = "56.0", z="60.92", title="SLA Calls" },
                    new DataEntity { x = "6PM", y = "34.0", z="60.92", title="SLA Calls" },
                    new DataEntity { x = "7PM", y = "23.0", z="60.92", title="SLA Calls" },
                    new DataEntity { x = "8PM", y = "67.0", z="60.92", title="SLA Calls" },
                    new DataEntity { x = "9PM", y = "34.0", z="60.92", title="SLA Calls" },

                    new DataEntity { x = "1PM", y = "42.5", z="60.92", title="Overall SLA" },
                    new DataEntity { x = "2PM", y = "44.3", z="60.92", title="Overall SLA" },
                    new DataEntity { x = "3PM", y = "28.7", z="60.92", title="Overall SLA" },
                    new DataEntity { x = "4PM", y = "32.7", z="60.92", title="Overall SLA" },
                    new DataEntity { x = "5PM", y = "75.0", z="60.92", title="Overall SLA" },
                    new DataEntity { x = "6PM", y = "45.0", z="60.92", title="Overall SLA" },
                    new DataEntity { x = "7PM", y = "23.0", z="60.92", title="Overall SLA" },
                    new DataEntity { x = "8PM", y = "56.0", z="60.92", title="Overall SLA" },
                    new DataEntity { x = "9PM", y = "23.0", z="60.92", title="Overall SLA" },

                    new DataEntity { x = "1PM", y = "56.5", z="60.92", title="Volumn" },
                    new DataEntity { x = "2PM", y = "37.7", z="60.92", title="Volumn" },
                    new DataEntity { x = "3PM", y = "34.7", z="60.92", title="Volumn" },
                    new DataEntity { x = "4PM", y = "67.5", z="60.92", title="Volumn" },
                    new DataEntity { x = "5PM", y = "45.0", z="60.92", title="Volumn" },
                    new DataEntity { x = "6PM", y = "12.0", z="60.92", title="Volumn" },
                    new DataEntity { x = "7PM", y = "34.0", z="60.92", title="Volumn" },
                    new DataEntity { x = "8PM", y = "34.0", z="60.92", title="Volumn" },
                    new DataEntity { x = "9PM", y = "23.0", z="60.92", title="Volumn" },
                };
                    return Json(new { Title = "Lấy dữ liệu thành công", Error = false, Object = data.GroupBy(x => x.title).ToList() });
                }
                else
                {
                    var data = new List<DataEntity>
                {
                    new DataEntity { x = "1PM", y = "85.4", z="60.92", title="SLA - Buzz" },
                    new DataEntity { x = "2PM", y = "78.7", z="60.92", title="SLA - Buzz" },
                    new DataEntity { x = "3PM", y = "23.9", z="60.92", title="SLA - Buzz" },
                    new DataEntity { x = "4PM", y = "12.0", z="60.92", title="SLA - Buzz" },
                    new DataEntity { x = "5PM", y = "46.0", z="78.92", title="SLA - Buzz" },
                    new DataEntity { x = "6PM", y = "78.0", z="78.92", title="SLA - Buzz" },
                    new DataEntity { x = "7PM", y = "56.0", z="78.92", title="SLA - Buzz" },
                    new DataEntity { x = "8PM", y = "23.0", z="78.92", title="SLA - Buzz" },
                    new DataEntity { x = "9PM", y = "45.0", z="78.92", title="SLA - Buzz" },

                    new DataEntity { x = "1PM", y = "32.3", z="60.92", title="SLA Calls" },
                    new DataEntity { x = "2PM", y = "33.9", z="60.92", title="SLA Calls" },
                    new DataEntity { x = "3PM", y = "56.3", z="60.92", title="SLA Calls" },
                    new DataEntity { x = "4PM", y = "23.6", z="60.92", title="SLA Calls" },
                    new DataEntity { x = "5PM", y = "34.0", z="60.92", title="SLA Calls" },
                    new DataEntity { x = "6PM", y = "45.0", z="60.92", title="SLA Calls" },
                    new DataEntity { x = "7PM", y = "23.0", z="60.92", title="SLA Calls" },
                    new DataEntity { x = "8PM", y = "65.0", z="60.92", title="SLA Calls" },
                    new DataEntity { x = "9PM", y = "34.0", z="60.92", title="SLA Calls" },

                    new DataEntity { x = "1PM", y = "42.5", z="60.92", title="Overall SLA" },
                    new DataEntity { x = "2PM", y = "44.3", z="60.92", title="Overall SLA" },
                    new DataEntity { x = "3PM", y = "34.7", z="60.92", title="Overall SLA" },
                    new DataEntity { x = "4PM", y = "32.7", z="60.92", title="Overall SLA" },
                    new DataEntity { x = "5PM", y = "23.0", z="60.92", title="Overall SLA" },
                    new DataEntity { x = "6PM", y = "44.0", z="60.92", title="Overall SLA" },
                    new DataEntity { x = "7PM", y = "23.0", z="60.92", title="Overall SLA" },
                    new DataEntity { x = "8PM", y = "34.0", z="60.92", title="Overall SLA" },
                    new DataEntity { x = "9PM", y = "23.0", z="60.92", title="Overall SLA" },

                    new DataEntity { x = "1PM", y = "56.5", z="60.92", title="Volumn" },
                    new DataEntity { x = "2PM", y = "37.7", z="60.92", title="Volumn" },
                    new DataEntity { x = "3PM", y = "23.7", z="60.92", title="Volumn" },
                    new DataEntity { x = "4PM", y = "44.5", z="60.92", title="Volumn" },
                    new DataEntity { x = "5PM", y = "55.0", z="60.92", title="Volumn" },
                    new DataEntity { x = "6PM", y = "12.0", z="60.92", title="Volumn" },
                    new DataEntity { x = "7PM", y = "54.0", z="60.92", title="Volumn" },
                    new DataEntity { x = "8PM", y = "32.0", z="60.92", title="Volumn" },
                    new DataEntity { x = "9PM", y = "12.0", z="60.92", title="Volumn" },
                };
                    return Json(new { Title = "Lấy dữ liệu thành công", Error = false, Object = data.GroupBy(x => x.title).ToList() });
                }                
            }
            catch (Exception ex)
            {
                return Json(new { Title = "Lấy dữ liệu không thành công", Error = true, Object = ex.Message });
            }
        }
        public class DataEntity
        {
            public string x { set; get; }
            public string y { set; get; }
            public string z { set; get; }
            public string title { set; get; }
        }
    }
}